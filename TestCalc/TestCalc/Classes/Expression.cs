﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalc.Classes
{
	/// <summary>
	/// Элемент дерева выражений
	/// </summary>
	public class Expression
	{
		/// <summary>
		/// Первый операнд выражения
		/// </summary>
		public Expression FirstOperand = null;
		/// <summary>
		/// Второй операнд выражения
		/// </summary>
		public Expression SecondOperand = null;
		/// <summary>
		/// Родительский узел выражения
		/// </summary>
		public Expression ParentExpression = null;
		/// <summary>
		/// Тип операции выражения
		/// </summary>
		public OperationType Operation;
		/// <summary>
		/// Числовое значение для константного выражения
		/// </summary>
		public decimal NumberValue = 0m;
		/// <summary>
		/// Признак отрицательного выражения
		/// </summary>
		public bool UnaryNegate = false;

		/// <summary>
		/// Вставить узел перед текущим(текущий узел становится первым операндом вставляемого, вставляемый становится родителем текущего)
		/// </summary>
		/// <param name="expressionNode"></param>
		public void InsertBefore(Expression expressionNode)
		{
			expressionNode.FirstOperand = this;
			if (ParentExpression != null)
			{
				expressionNode.ParentExpression = ParentExpression;
				expressionNode.ParentExpression.SecondOperand = expressionNode;
			}
			ParentExpression = expressionNode;
		}

		/// <summary>
		/// Вставить узел после текущиего(вставляемый узел становится вторым операндом и потомком текущего, второй операнд текущего узла копируется в первый операнд вставляемого)
		/// </summary>
		/// <param name="expressionNode"></param>
		public void InsertAfter(Expression expressionNode)
		{
			expressionNode.FirstOperand = SecondOperand;
			expressionNode.ParentExpression = this;
			SecondOperand = expressionNode;
		}

		public decimal GetValue()
		{
			decimal curValue = NumberValue; //получить значение константного выражения
			if (FirstOperand != null) curValue = (FirstOperand.UnaryNegate) ? -FirstOperand.GetValue() : FirstOperand.GetValue();

			if (SecondOperand != null)
			{
				try
				{
					var value = (SecondOperand.UnaryNegate) ? -SecondOperand.GetValue() : SecondOperand.GetValue();

					switch (Operation)
					{
						case OperationType.Add:
							curValue += value;
							break;
						case OperationType.Substract:
							curValue -= value;
							break;
						case OperationType.Multiply:
							curValue *= value;
							break;
						case OperationType.Divide:
							curValue /= value;
							break;
					}
				}
				catch (DivideByZeroException)
				{
					throw new CalcException("Ошибка деления на ноль", curValue, SecondOperand.GetValue(), Operation);
				}
				catch (OverflowException)
				{
					throw new CalcException("В результате вычислений значение вышло из границ допустимого диапазона", curValue, SecondOperand.GetValue()
						, Operation);
				}
			}

			return curValue;
		}

		public override string ToString()
		{
			string result = "";

			if (UnaryNegate) result += "-";
			result += FirstOperand?.ToString();

			switch (Operation)
			{
				case OperationType.Add: result += " + "; break;
				case OperationType.Substract: result += " - "; break;
				case OperationType.Multiply: result += " * "; break;
				case OperationType.Divide: result += " / "; break;
				case OperationType.Number: result += NumberValue; break;
			}

			result += SecondOperand?.ToString();

			return result;
		}
	}

	public enum OperationType
	{
		Number,
		Add,
		Substract,
		Multiply,
		Divide
	}

	public static class OperationExt
	{
		public static string GetName(this OperationType me)
		{
			switch (me)
			{
				case OperationType.Add: return "Сложение";
				case OperationType.Substract: return "Вычитание";
				case OperationType.Multiply: return "Умножение";
				case OperationType.Divide: return "Деление";
				default: return me.ToString();
			}
		}
	}
}
