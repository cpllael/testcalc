﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalc.Classes
{
	public static class Parser
	{
		/// <summary>
		/// Разделитель целых и дробных частей
		/// </summary>
		public static char NumberSeparator = '.';

		/// <summary>
		/// Построить дерево выражений из входной строки
		/// </summary>
		public static Expression ConstructExpression(string input)
		{
			if (string.IsNullOrEmpty(input)) throw new ArgumentNullException(nameof(input));

			return ConstructExpression(input, 0, input.Length - 1);
		}

		public static Expression ConstructExpression(string input, int startPos, int endPos)
		{
			if (string.IsNullOrEmpty(input)) throw new ArgumentNullException(nameof(input));

			Expression currentExpression = new Expression();
			bool firstOperandRead = false, secondOperandRead = false, operationRead = false;
			bool shouldNegateNextExpr = false;

			for (int n = startPos; n <= endPos; n++)
			{
				switch (input[n])
				{
					case '0':case '1':case '2':case '3':case '4':
					case '5':case '6':case '7':case '8':case '9':

						//считать число и передвинуть индекс символа в строке на следующий после числа символ
						decimal curVal = ParseNumber(input, n, out int endNumPos);
						n = endNumPos - 1;

						if (firstOperandRead == false)
						{// поместить число в первый операнд выражения если не было считано
							currentExpression.FirstOperand = new Expression
							{
								NumberValue = curVal,
								Operation = OperationType.Number,
								ParentExpression = currentExpression,
								UnaryNegate = shouldNegateNextExpr
							};

							firstOperandRead = true; //признак того, что первый операнд прочитан
						}

						if (operationRead && secondOperandRead == false)
						{
							currentExpression.SecondOperand = new Expression
							{
								NumberValue = curVal,
								Operation = OperationType.Number,
								ParentExpression = currentExpression,
								UnaryNegate = shouldNegateNextExpr
							};

							secondOperandRead = true; //признак того, что второй операнд прочитан
						}

						//очистить флаг признака отрицательного выражения
						if (shouldNegateNextExpr) { shouldNegateNextExpr = false; }

						break;

					case '+':
						if (firstOperandRead && operationRead == false && !secondOperandRead)
						{//после успешного считываний первого операнда, установить тип операции у текущего выражения
							currentExpression.Operation = OperationType.Add;
							operationRead = true; //флаг того, что операция считана
						}
						else if (secondOperandRead)
						{ //если текущее выражение заполнено(считаны оба операнда), создать новое выражение 
							var newExpr = new Expression() { Operation = OperationType.Add };

							if (currentExpression.ParentExpression != null)
							{//если у предыдущего выражения тип операции более приоритетный, пройти вверх по дереву до ближайшего низкоприоритетного выражений
								for (; currentExpression.Operation == OperationType.Multiply || currentExpression.Operation == OperationType.Divide
									; currentExpression = currentExpression.ParentExpression) ;

								//вставить новое выражение после ближайшего низкоприоритетного (+/-) 
								currentExpression.InsertBefore(newExpr);
							}
							else newExpr.FirstOperand = currentExpression;
							
							currentExpression = newExpr;// пометить новое выражение как текущее и очистить флаг того, что второго операнд считан
							secondOperandRead = false;
						}

						break;
					case '-':
						if (firstOperandRead == true && operationRead == false && !secondOperandRead)
						{
							currentExpression.Operation = OperationType.Substract;
							operationRead = true;
							break;
						}
						else if (secondOperandRead)
						{
							var newExpr = new Expression() { Operation = OperationType.Substract };

							if (currentExpression.ParentExpression != null)
							{
								for (; currentExpression.Operation == OperationType.Multiply || currentExpression.Operation == OperationType.Divide
									; currentExpression = currentExpression.ParentExpression) ;

								currentExpression.InsertBefore(newExpr);
							}
							else newExpr.FirstOperand = currentExpression;
							
							currentExpression = newExpr;
							secondOperandRead = false;
							break;
						}

						if (shouldNegateNextExpr) shouldNegateNextExpr = false;
						else
							shouldNegateNextExpr = !firstOperandRead || (!secondOperandRead && operationRead);

						break;
					case '*':
						if (!firstOperandRead || operationRead && !secondOperandRead) RaiseInvalidChar(input, n);

						if (firstOperandRead && !secondOperandRead)
						{
							currentExpression.Operation = OperationType.Multiply;
							operationRead = true;
						}
						else if (secondOperandRead)
						{
							var newExpr = new Expression();
							newExpr.Operation = OperationType.Multiply;

							if (currentExpression.Operation == OperationType.Add || currentExpression.Operation == OperationType.Substract)
								//если текущее выражение низкоприоритетное, то вставить новое выражение после текущего
								//(более приоритетные выражения размещаются ближе к листьям дерева)
								currentExpression.InsertAfter(newExpr);
							else
								currentExpression.InsertBefore(newExpr);

							currentExpression = newExpr;
							secondOperandRead = false;
						}

						break;
					case '/':
						if (!firstOperandRead || operationRead && !secondOperandRead) RaiseInvalidChar(input, n);
						if (firstOperandRead && !secondOperandRead)
						{
							currentExpression.Operation = OperationType.Divide;
							operationRead = true;
						}
						else if (secondOperandRead)
						{
							var newExpr = new Expression();
							newExpr.Operation = OperationType.Divide;

							if (currentExpression.Operation == OperationType.Add || currentExpression.Operation == OperationType.Substract)
								currentExpression.InsertAfter(newExpr);
							else
								currentExpression.InsertBefore(newExpr);

							currentExpression = newExpr;
							secondOperandRead = false;
						}

						break;
					case '(':
						if (firstOperandRead && !operationRead || secondOperandRead) RaiseInvalidChar(input, n);

						//найти закрывающую скобку и вызвать конструктор дерева выражений для строки внутри скобок
						int closeBracketPos = FindCloseBracket(input, n); 
						Expression subExp = ConstructExpression(input.Substring(n + 1, closeBracketPos - n - 1));
						n = closeBracketPos;

						//поместить считанное дерево выражений в первый или второй операнд текущего узла(по аналогии с константным выражением)
						if (firstOperandRead == false)
						{
							currentExpression.FirstOperand = subExp;
							subExp.ParentExpression = currentExpression;
							subExp.UnaryNegate = shouldNegateNextExpr;

							firstOperandRead = true;
						}

						if (operationRead && secondOperandRead == false)
						{
							currentExpression.SecondOperand = subExp;
							subExp.ParentExpression = currentExpression;
							subExp.UnaryNegate = shouldNegateNextExpr;

							secondOperandRead = true;
						}

						if (shouldNegateNextExpr) { shouldNegateNextExpr = false; }
						
						break;
					case ')':
						RaiseInvalidChar(input, n);
						break;
					default:
						RaiseInvalidChar(input, n);
						break;
				}
			}

			if (operationRead && secondOperandRead == false)
				throw new ParseException("Не найден операнд", input, endPos);

			if (firstOperandRead == false) throw new ParseException("Не найден операнд", input, startPos);

			//возвратить корневой узел дерева выражений
			for (; currentExpression.ParentExpression != null; currentExpression = currentExpression.ParentExpression) ;
			return currentExpression;
		}

		static void RaiseInvalidChar(string input, int n)
		{
			throw new ParseException("Не ожидалось '" + input[n] + "' в этой позиции", input, n);
		}

		/// <summary>
		/// Из входной строки считать число
		/// </summary>
		/// <param name="startPos">Индекс первого символа числа</param>
		/// <param name="numberEndPosition">Возвращаемый индекс символа сразу после последнего символа числа</param>
		/// <returns></returns>
		public static decimal ParseNumber(string input, int startPos, out int numberEndPosition)
		{
			decimal result = 0m;
			int curPos = startPos;

			for (; curPos < input.Length && (input[curPos] >= '0' && input[curPos] <= '9' || input[curPos] == NumberSeparator)
				; curPos++) ; //найти позицию, где кончается число

			string subnum = "";
			try
			{
				result = decimal.Parse(subnum = input.Substring(startPos, curPos - startPos));
				numberEndPosition = curPos;
			}
			catch (Exception ex)
			{
				throw new ParseException("Не удалось распознать число: " + subnum + "\n " + ex.Message, input, startPos);
			}

			return result;
		}

		/// <summary>
		/// Найти закрывающую скобку
		/// </summary>
		/// <param name="startPos">Индекс символа открывающей скобки</param>
		/// <returns>Индекс позиции закрывающей скобки</returns>
		public static int FindCloseBracket(string input, int startPos)
		{
			if (string.IsNullOrEmpty(input)) throw new ArgumentNullException(nameof(input));
			if (input[startPos] != '(') throw new ParseException("Ошибка обработки строки: в этой позиции ожидалось '('", input, startPos);
			int countOpenedBrackets = 1, countClosedBrackets = 0;

			for (int n = startPos + 1; n < input.Length; n++)
			{
				if (input[n] == '(') { countOpenedBrackets++; continue; }
				if (input[n] == ')') { countClosedBrackets++; }

				if (countOpenedBrackets == countClosedBrackets)
					return n;
			}

			throw new ParseException("Не найдена закрывающая скобка", input, startPos);
		}
	}
}
