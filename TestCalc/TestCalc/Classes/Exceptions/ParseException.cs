﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalc.Classes
{
	/// <summary>
	/// Ошибка разбора входной строки и построения выражения
	/// </summary>
	public class ParseException: ApplicationException
	{
		public ParseException(string message, string stringToParse, int invalidCharacterIndex) : base(message)
		{
			StringToParse = stringToParse;
			InvalidCharacterIndex = invalidCharacterIndex;
		}

		public string StringToParse;
		public int InvalidCharacterIndex;
	}
}
