﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalc.Classes
{
	/// <summary>
	/// Ошибка при вычислении выражения
	/// </summary>
	public class CalcException : ApplicationException
	{
		public CalcException(string message, decimal operand1, decimal operand2, OperationType operation) : base(message)
		{
			Operand1 = operand1;
			Operand2 = operand2;
			Operation = operation;
		}

		public decimal Operand1;
		public decimal Operand2;
		public OperationType Operation;
	}
}
