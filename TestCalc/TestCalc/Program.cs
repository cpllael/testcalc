﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestCalc.Classes;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;

namespace TestCalc
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Тестовый калькулятор");

			if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator.Length == 1)
			{
				Parser.NumberSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
				Console.WriteLine("В качестве разделителя целых и дробных частей используется: '" + Parser.NumberSeparator + "' (системная настройка, команды setdot/setcomma изменяют разделитель по умолчанию)");
			}
			else
			{
				ChangeNumberSeparator('.');
				Console.WriteLine("Команды setdot/setcomma изменяют разделитель по умолчанию)");
			}

			Console.WriteLine("Для выхода из программы введите quit или exit");

			var reInvalidRemoval = new Regex(@"([^0-9*+\-()/" + Parser.NumberSeparator + "])");
			string input;

			while (true)
			{
				Console.Write(">");
				input = Console.ReadLine().Replace(" ", "");
				if (input.ToLower() == "quit" || input.ToLower() == "exit") break;

				if (input.ToLower() == "setdot") { ChangeNumberSeparator('.'); reInvalidRemoval = new Regex(@"([^0-9*+\-()/" + Parser.NumberSeparator + "])"); continue; }
				if (input.ToLower() == "setcomma") { ChangeNumberSeparator(','); reInvalidRemoval = new Regex(@"([^0-9*+\-()/" + Parser.NumberSeparator + "])"); continue; }

				var matches = reInvalidRemoval.Matches(input); //найти все символы кроме чисел, арифм действий и скобок

				if (matches.Count != 0)
				{
					string invalidChars = "", comma = "";
					bool possiblePeriod = false;
					foreach (var match in matches)
					{
						string m = match.ToString();
						invalidChars += comma + m;
						comma = "', '";
						if (m == "." || m == ",") possiblePeriod = true;
					}
					Console.WriteLine("\nСледующие символы были удалены из входной строки: '" + invalidChars + "'");
					if (possiblePeriod)
					{
						Console.WriteLine("В качестве разделителя целых и дробных частей используется: '" + Parser.NumberSeparator + "'");
						possiblePeriod = false;
					}

					input = reInvalidRemoval.Replace(input, "");//убрать все символы кроме чисел, арифм действий и скобок

				}

				while (input.IndexOf("()") != -1) input = input.Replace("()", ""); //убрать пустые скобки

				if (input.Length != 0)
				{
					Console.WriteLine("\nРезультат вычисления " + input + " : ");

					try
					{
						Expression expr = Parser.ConstructExpression(input);//построить выражение из входной строки
						Console.WriteLine(expr.GetValue());
					}
					catch (ParseException ex)
					{
						Console.WriteLine(ex.Message);
						if (string.IsNullOrEmpty(ex.StringToParse) == false && ex.InvalidCharacterIndex >= 0)
						{
							Console.WriteLine(ex.StringToParse);
							Console.WriteLine(new string(' ', ex.InvalidCharacterIndex) + "^");//вывести указатель позиции некорректного символа
						}

					}
					catch (CalcException ex)
					{
						Console.WriteLine(ex.Message);
						Console.WriteLine("Операция: " + ex.Operation.GetName() + " " + ex.Operand1 + " и " + ex.Operand2);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Непредвиденная ошибка:\n " + ex.Message);
					}
				}
			}

		}

		/// <summary>
		/// Изменить культурные настройки текущего потока и установить разделитель целых/дробных частей
		/// </summary>
		/// <param name="separator">Символ разделитель</param>
		static void ChangeNumberSeparator(char separator)
		{
			string CultureName = Thread.CurrentThread.CurrentCulture.Name;
			CultureInfo ci = new CultureInfo(CultureName);

			ci.NumberFormat.NumberDecimalSeparator = separator.ToString();
			Thread.CurrentThread.CurrentCulture = ci;

			Parser.NumberSeparator = separator;
			Console.WriteLine("Установлен разделитель '" + separator + "'");
		}
	}


}
